package blablubbot.race;

import blablubbot.telemetry.car.Car;
import blablubbot.telemetry.track.Track;

public class Race {

  public Track track;
  public Car[] cars;
  public Object raceSession;

  public Race(final Track track, final Car[] cars, final Object raceSession) {
    this.track = track;
    this.cars = cars;
    this.raceSession = raceSession;
  }
}
