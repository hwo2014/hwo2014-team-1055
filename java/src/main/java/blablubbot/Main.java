package blablubbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import blablubbot.bot.BlablubBrain;
import blablubbot.bot.BotId;
import blablubbot.messages.CreateRace;
import blablubbot.messages.Join;
import blablubbot.messages.MsgWrapper;
import blablubbot.messages.Ping;
import blablubbot.messages.SendMsg;
import blablubbot.messages.Switch;
import blablubbot.messages.Throttle;
import blablubbot.messages.Turbo;
import blablubbot.race.Race;
import blablubbot.telemetry.car.CarId;
import blablubbot.telemetry.car.CarPosition;
import blablubbot.telemetry.track.Track;

import com.google.gson.Gson;

public class Main {
  
    static String trackName = null;
    static String botKey = null;
    static String botName = null;
  
    public static void main(String... args) throws IOException {
//        String host = "webber.helloworldopen.com";
//        int port = 8091;
//        String botName = "blablub";
//        String botKey = "ydQccQ6VaTxPBg";
    	String host = args[0];
    	int port =  Integer.parseInt(args[1]);
    	botName = args[2];
    	botKey = args[3];
    	
    	if (args.length > 4) {
    	  trackName = args[4];
    	}

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    List<CarPosition> carPositions = new ArrayList<CarPosition>();
    static boolean turboAvailable = false;
    
    
    
    Track track;
    
    
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        LinkedList<CarPosition[]> carPositionQueue = new LinkedList<CarPosition[]>();
        
        if (trackName != null) {
          CreateRace createRace = new CreateRace(new BotId(botName, botKey), trackName, null, 1);
          send(createRace);
        } else {
          send(join);
        }
        
        int chaos = 0;
        boolean left = true;
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
               CarPosition[] carPositions = gson.fromJson(gson.toJson(msgFromServer.data), CarPosition[].class);
               for (CarPosition cp : carPositions) {
                 cp.myCar();
               }
               carPositionQueue.addFirst(carPositions);
               if (BlablubBrain.maxSpeed == 0) {
            	   send(new Throttle(BlablubBrain.getMaxSpeed(carPositionQueue, this.track)));
               } else {
//            	   System.out.printf("MaxSpeed: %f\n", BlablubBrain.maxSpeed);
            	   if ((chaos % 42) == 0) {
            	     send(new Switch(left));
            	     System.out.printf("Switch: %b\n", left);
            	     if ((chaos / 13) % 3 == 0) {
            	       left = !left;
            	     }
            	   } else {
            	     send(new Throttle(BlablubBrain.doThrottle(carPositionQueue, this.track)));
            	   }
            	   chaos++;
               }
               if(turboAvailable == true) {
              	 for (CarPosition cp : carPositionQueue.getFirst()) {
           			if (cp.we) {
           				if(BlablubBrain.useTurbo(track, cp)) {
           					send(new Turbo());
           					System.out.println("TUUUURRRRBOOOOOOO... go 4 blablub!");
           					turboAvailable = false;
           				}
           			}
              	 }
               }
 
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                RaceWrapper raceWrapper = gson.fromJson(gson.toJson(msgFromServer.data), RaceWrapper.class);
                initTrack(raceWrapper.race);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("crash")) {
            	if (BlablubBrain.maxAngle  == 0.0) {
            	  CarId carId = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
            	  System.out.printf("MaxAngle: %f \n", BlablubBrain.getMaxAngle(carPositionQueue, this.track, carId));
            	}
            	System.out.println("crash - wait 5 sec");
            } else if (msgFromServer.msgType.equals("spawn")) {
                System.out.println("Car has respawned");
                send(new Throttle(1.0));
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
            	turboAvailable = true;          
            	System.out.println("GOT TURBO! Use it!");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Throttle(1.0));
            } else {
//                send(new Ping());
            }
//            System.out.print(BlablubBrain.throttle);
//	    	System.out.println(msgFromServer.data);
        }
    }
    
    

   private void initTrack(Race race) {
      this.track = race.track;
      this.track.calcLength();
   
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

class RaceWrapper {
  public final Race race;
  
  RaceWrapper(Race race) {
    this.race = race;  
  }
}
