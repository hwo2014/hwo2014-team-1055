package blablubbot.telemetry.car;

public class CarId {
  public final String name;
  public final String color;
  
  public CarId(String name, String color) {
    this.name = name;
    this.color = color;
  }
}
