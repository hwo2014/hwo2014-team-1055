package blablubbot.telemetry.car;

public class Dimensions {
  private final Float length;
  private final Float width;
  private final Float guideFlagPosition;
  
  public Dimensions(Float length, Float width, Float guideFlagPosition) {
    this.length = length;
    this.width = width;
    this.guideFlagPosition = guideFlagPosition;
    
  }
}
