package blablubbot.telemetry.car;

import blablubbot.Config;
import blablubbot.telemetry.track.PiecePosition;

public class CarPosition {
  public CarId id;
  public float angle;
  public PiecePosition piecePosition;
  public int lap;
  public boolean we;

  public CarPosition(CarId id, float angle, PiecePosition piecePosition, int lap) {
    this.id = id;
    this.angle = angle;
    this.piecePosition = piecePosition;
    this.lap = lap;
  }

  public void myCar() {
    if (id.name.contains(Config.CAR_NAME)) {
      this.we = true;
    }
  }
}