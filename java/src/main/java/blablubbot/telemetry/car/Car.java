package blablubbot.telemetry.car;

public class Car {

	public CarId carId;
	public Dimensions dimensions;
		
	public Car (CarId carId, Dimensions dimensions) {	
		this.carId = carId;
		this.dimensions = dimensions;
	}
	
}
