package blablubbot.telemetry.track;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Track {
  
  public final String id;
  public final String name;
  public final List<Piece> pieces;
  public final float lapLength;
  public final List<Lane> lanes;
  
  
  public Track(final String id, final String name, final List<Piece> pieces, List<Lane> lanes) {
    this.id = id;
    this.name = name;
    this.pieces = pieces;
    this.lapLength = this.lapLength(); //initialize lapLength
    this.lanes = lanes;
  }
  
  public Piece createStraight(Float length) {
    return new Piece(length, null, null, false);
  }
  
  public Piece createCurve(Float radius, Float angle) {
    return new Piece(null, radius, angle, false);
  }
  
  public Piece createStraightSwitcher(Float length) {
    return new Piece(length, null, null, true);
  }
  
  public Piece createCurveSwitcher(Float radius, Float angle) {
    return new Piece(null, radius, angle, true);
  }
  
  public void calcLength() {
    for(Piece piece : this.pieces) {
      piece.laneLenghts = new HashMap<Integer, Float>();
      if (piece.radius != null && piece.angle != null) {
        piece.length = (float) (Math.abs(piece.angle)/360 * 2* piece.radius * Math.PI);
        for (Lane lane: lanes) {
          piece.laneLenghts.put(lane.index, (float) (Math.abs(piece.angle)/360 * 2* (piece.radius + lane.distanceFromCenter * -1 * Math.signum(piece.angle)) * Math.PI));
        }
      } else {
        for (int i = 0; i<lanes.size(); i++) {
          piece.laneLenghts.put(i, piece.length);
        }
      }
      System.out.printf("%f, %f, %f \n", piece.length, piece.angle, piece.radius);
    }
  }
  public float lapLength() {
    float length = 0;
    for(Piece piece : this.pieces){
      length += piece.length;
    }
    return length;
  }
}
