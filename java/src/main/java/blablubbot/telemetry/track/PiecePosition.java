package blablubbot.telemetry.track;

import blablubbot.telemetry.car.Lane;

public class PiecePosition {
  public int pieceIndex;
  public float inPieceDistance;
  public Lane lane;
  
  public PiecePosition(int pieceIndex, float inPieceDistance, Lane lane) {
    this.pieceIndex = pieceIndex;
    this.inPieceDistance = inPieceDistance; 
    this.lane = lane;
  }
}
