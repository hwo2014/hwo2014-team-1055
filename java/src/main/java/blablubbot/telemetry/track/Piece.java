package blablubbot.telemetry.track;

import java.util.HashMap;
import java.util.List;

public class Piece {
  
  public Float length;
  public Float radius;
  public Float angle;
  public boolean switcher;
  public HashMap<Integer, Float> laneLenghts;
  
  public Piece(Float length, Float radius, Float angle, boolean switcher) {
    this.length = length;
    this.radius = radius;
    this.angle = angle;
    this.switcher = switcher;
    
  }
}