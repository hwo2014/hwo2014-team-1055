package blablubbot.messages;

public class Join extends SendMsg {
  public String name;
  public String key;

  public Join(String name, String key) {
    this.name = name;
    this.key = key;
  }

  @Override
  protected String msgType() {
    return "join";
  }
}