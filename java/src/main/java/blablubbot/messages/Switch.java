package blablubbot.messages;

public class Switch extends SendMsg {
    private boolean left;

    public Switch(boolean left) {
        this.left = left;
    }

    @Override
    protected Object msgData() {
        if (this.left) {
          return "Left";
        } else {
          return "Right";
        }
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}