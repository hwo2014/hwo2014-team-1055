package blablubbot.messages;

import blablubbot.bot.BotId;

public class CreateRace extends SendMsg {
  public final BotId botId;
  public final String trackName;
  public final String password;
  public final int carCount;
  
  public CreateRace(BotId botId, String trackName, String password, int carCount) {
    this.botId=botId;
    this.trackName=trackName;
    this.password=password;
    this.carCount=carCount;
  }
 
 @Override
 protected String msgType() {
   return "createRace";
 }
 
}