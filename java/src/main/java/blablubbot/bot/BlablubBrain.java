package blablubbot.bot;

import java.util.LinkedList;
import java.util.List;

import blablubbot.telemetry.car.CarId;
import blablubbot.telemetry.car.CarPosition;
import blablubbot.telemetry.track.Lane;
import blablubbot.telemetry.track.Piece;
import blablubbot.telemetry.track.Track;


/* here should be all the things interfering with the acceleration 
 * throttle control, forecast and so on
 */

public class BlablubBrain {
	public static float formerSpeed = 0;
	public static float maxSpeed = 0;
	public static float friction = 0;
	public static float maxAngle = 0;
	
	public static Double throttle = 1.00;
	
	public static Double throttleSharp = 0.1;
	public static Double throttleUnSharp = 0.4;
	public static Double throttleStandard = 0.5;
	
	BlablubBrain(Track track, Double throttle) {
		this.throttle = throttle;
	}
	
	public static Double doThrottle(LinkedList<CarPosition[]> carPositionQeue, Track track) {
    
		for (CarPosition cp : carPositionQeue.getFirst()) {
			if (cp.we) {
				if (thisPiece(track, cp).radius != null
            && sharpCurve(track, cp)
            && Math.abs(cp.angle) > maxAngle * 72 / 100) {
					throttle = 0.12;
				} else if (thisPiece(track, cp).radius != null
						&& !sharpCurve(track, cp)
						&& Math.abs(cp.angle) > maxAngle * 72 / 100) {
					throttle = 0.62;
				} else if (nextPiece(track, cp).radius == null
						&& Math.abs(cp.angle) < maxAngle * 42 / 100) {
					throttle = 1.0;
				} else if ((previousPiece(track, cp).radius == null)
						&& (thisPiece(track, cp).radius == null)
						&& nextPiece(track, cp).radius != null
						&& Math.abs(nextPiece(track, cp).radius) > 0.0) {
					throttle = 0.00;
				} else {
				  throttle = 0.82;
				}
				System.out.printf("Throttle: %f\n", throttle);
				return throttle;
			}
		}
		return throttle;
	}
	
	public static Piece thisPiece(Track track, CarPosition cp) {
    return track.pieces.get(((cp.piecePosition.pieceIndex%(track.pieces.size()-1)-1)+track.pieces.size()-1)%(track.pieces.size()-1));
	}
	
	public static Piece nextPiece(Track track, CarPosition cp) {
	  return track.pieces.get(cp.piecePosition.pieceIndex%(track.pieces.size()-1)+1);
	}
	
	public static Piece previousPiece(Track track, CarPosition cp) {
    return track.pieces.get(((cp.piecePosition.pieceIndex%(track.pieces.size()-1)-2)+track.pieces.size()-1)%(track.pieces.size()-1));
  }
	
	public static Piece offsetPiece(Track track, CarPosition cp, int offset) {
    return track.pieces.get(((cp.piecePosition.pieceIndex%(track.pieces.size()-1)+(offset-1))+track.pieces.size()-1)%(track.pieces.size()-1));
  }
	
	
	
	
	public static float getSpeed(LinkedList<CarPosition[]> carPositionQueue, Track track) {
		float currentInPieceDistance = 0;
		float formerInPieceDistance = 0;
		int currentPieceIndex = 0;
		int formerPieceIndex = 0;
		int currentLane = 0; 
		
		float speed = 0;
		float angle = 0;	
		float acceleration = 0;
		
		
		for (CarPosition cp : carPositionQueue.getFirst()) {
			if (cp.we) {
				currentPieceIndex = cp.piecePosition.pieceIndex;
				currentInPieceDistance = cp.piecePosition.inPieceDistance;
				currentLane = cp.piecePosition.lane.endLaneIndex;
				angle = cp.angle;
			}
		}
		if (carPositionQueue.size() > 1) {
			for (CarPosition cp : carPositionQueue.get(1)) {
				if (cp.we) {
					formerPieceIndex = cp.piecePosition.pieceIndex; 
					formerInPieceDistance = cp.piecePosition.inPieceDistance;
				}
			}
		}
		if(currentPieceIndex == formerPieceIndex) {
			speed = currentInPieceDistance - formerInPieceDistance;
		} else {
			speed = track.pieces.get(formerPieceIndex).laneLenghts.get(currentLane) - formerInPieceDistance + currentInPieceDistance;
		}
		acceleration = speed - formerSpeed; 
		
		//System.out.println(acceleration);
		System.out.printf("Speed: %f, angle: %f, a: %f, formerLength: %f, fPD %d: %f, cPD %d: %f,  \n", speed, angle, acceleration,  track.pieces.get(formerPieceIndex).length, formerPieceIndex, formerInPieceDistance, currentPieceIndex, currentInPieceDistance);
		
		
		return speed;
	}
	
	public static float getMaxSpeed(LinkedList<CarPosition[]> carPositionQueue, Track track) {
		float currentSpeed = BlablubBrain.getSpeed(carPositionQueue, track);
		
		if (currentSpeed > 0.0 && (int) (currentSpeed *15) == (int) (formerSpeed *15)) {
		  maxSpeed = currentSpeed;
		  System.out.printf("maxSpeed: %f", maxSpeed);
		} else {
			formerSpeed = currentSpeed;
		}
		
		return (float) 1.0;
	}
	public static float getMaxAngle(LinkedList<CarPosition[]> carPositionQueue, Track track, CarId carId) {
		if (carPositionQueue.size() > 1) {
			for (CarPosition cp : carPositionQueue.get(0)) {
				if (cp.id.name.equals(carId.name)) {
					if (maxAngle == 0.0 || cp.angle > maxAngle) {
						maxAngle = cp.angle;
					} 
				}
			}
		}
		return Math.abs(maxAngle);
	}
	public static boolean sharpCurve (Track track, CarPosition cp) {
		Float curvature; 
		boolean stmt = false;
		
		if(thisPiece(track, cp).angle != null && thisPiece(track, cp).radius != null) {
			curvature = thisPiece(track, cp).angle / (thisPiece(track, cp).radius);
			if(Math.abs(curvature) > 0.5) {
				stmt = true;
			} 
		}
		return stmt;
	}
	
	public static Lane getCurrentLane(Track track, CarPosition cp) {
    
	  return track.lanes.get(cp.piecePosition.lane.endLaneIndex);
	  
	}
	public static boolean useTurbo(Track track, CarPosition cp) {
		boolean stmt = false;
		
		if(nextPiece(track,cp).angle == null && offsetPiece(track,cp, +2).angle == null) {
			if(nextPiece(track,cp).length+offsetPiece(track,cp, +2).length > 150) {
				stmt = true;
			}
		}		
		return stmt;
	}
	
}
